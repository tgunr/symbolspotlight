# README #

Create a Spotlight importer for object files and archives (.o, .a, and .dylib) files to permit using Spotlight to find imported and exported symbols.

* Version 1.2